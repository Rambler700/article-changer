# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 21:38:32 2019

@author: enovi
"""

import random
from nltk.corpus import wordnet

def main():
    choice = True
    article_text = ''
    c = 'y'
    print('Welcome to Article Changer!')
    while choice == True:
         c = input('Spin an article y/n? ')
         if c == 'y':
             article_text = get_text()
             min_length = get_length()
             spun_text = spin_text(article_text, min_length)
             print(spun_text)
             choice = True
         elif c == 'n':
             choice = False
             
def get_text():
    article_text = ''
    article_text = str(input('Enter the message here: '))
    return article_text

def get_length():
    min_length = 4
    min_length = int(input('Enter the minimum word length to modify: '))
    return min_length

def spin_text(article_text, min_length):
    
    spun_text = []
    article_text = article_text.split()
    choices = []
    
    for word in article_text:
        if word.islower() == True and len(word) >= min_length:
            for synset in wordnet.synsets(word): 
                for lemma in synset.lemmas():
                    choices.append(lemma.name())
            if len(choices) > 1:
                index = random.randint(0,len(choices)-1)                
                spun_text.append(choices[index])
            else:
                spun_text.append(word)
        else:
            spun_text.append(word)        
            
    spun_text = " ".join(spun_text)        
            
    return spun_text        
            
main()            
